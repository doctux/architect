echo -e "\033[1m\033[4m\033[46m\033[7mDefinition du nom d'utilisateur"
read -p 'Nom complet : ' namevar
read -p 'Username : ' uservar

echo -e "\033[1m\033[4m\033[41m\033[7mDefinition du nom de la machine"
read -p 'Nom de la machine : ' pcvar

echo -e "\033[1m\033[4m\033[46m\033[7mDefinition des passwords"
read -p 'Root Password : ' pwroot
read -p 'User Password : ' pwuser

parted /dev/sda -- mklabel gpt
parted /dev/sda -- mkpart ESP fat32 1MiB 512MiB            # /boot/efi
parted /dev/sda -- set 1 boot on
parted /dev/sda -- mkpart primary 512MiB 20GiB             # /
parted /dev/sda -- mkpart primary linux-swap 20GiB 22GiB   # swap
parted /dev/sda -- mkpart primary 22GiB 100%               # home
 
parted /dev/sda -- name 1 efi
parted /dev/sda -- name 2 root
parted /dev/sda -- name 3 swap
parted /dev/sda -- name 4 home
 
 
mkfs.fat -F32 /dev/disks/by-partlabel/efi
mkfs.ext4 /dev/disk/by-partlabel/home
mkfs.ext4 /dev/disk/by-partlabel/root
mkswap /dev/disk/by-partlabel/swap
swapon /dev/disk/by-partlabel/swap
 
 
mount /dev/disk/by-partlabel/root /mnt
mkdir -pv /mnt/{boot/efi,home}
mount /dev/disk/by-partlabel/efi /mnt/boot/efi
mount /dev/disk/by-partlabel/home /mnt/home

pacman -Syy --noconfirm
pacman -S reflector --noconfirm
reflector -c "FR" -f 12 -l 10 -n 12 --save /etc/pacman.d/mirrorlist
pacstrap /mnt base base-devel linux linux-firmware git nano zip unzip p7zip vim mc alsa-utils syslog-ng mtools dosfstools lsb-release ntfs-3g exfat-utils bash-completion neofetch xorg xfce4 xfce4-goodies grub efibootmgr zsh-completions
genfstab -U /mnt >> /mnt/etc/fstab


arch-chroot /mnt << CHOUCROUTE
timedatectl set-timezone Europe/Paris
sed -i "s/#fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g" /etc/locale.gen
locale-gen
echo LANG=fr_FR.UTF-8 > /etc/locale.conf
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg
systemctl enable gdm.service
systemctl enable NetworkManager.service
systemctl enable bluetooth
timedatectl set-ntp true
localedef -f UTF-8 -i fr_FR fr_FR.UTF-8
echo -e "\033[1m\033[4m\033[41m\033[7mDefinition du nom de la machine [CHROOT]"
echo $pcvar > /etc/hostname
echo 127.0.0.1   localhost > /etc/hosts
echo ::1         localhost >> /etc/hosts
echo 127.0.1.1	 $pcvar >> /etc/hosts


echo -e "\033[1m\033[4m\033[46m\033[7mDefinition du nom d'utilisateur [CHROOT]"

useradd -m -c "$namevar" -g users -G wheel -s /bin/bash $uservar
sed -i "s/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g" /etc/sudoers
echo -e "\033[1m\033[4m\033[46m\033[7mDefinition des passwords [CHROOT]"
echo Definir MDP de $uservar
echo "$uservar:$pwuser" | chpasswd
echo Definir MDP de root
echo "root:$pwroot" | chpasswd
echo neofetch >> /home/$uservar/.bashrc
CHOUCROUTE
exit
reboot now
